export const projects = [
    {
        title: "React Reserve",
        subtitle: "MERN Stack",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium dolore rerum laborum iure enim sint nemo omnis voluptate exercitationem eius?",
        image: "./project-1.gif",
        link: "https://reactbootcamp.com",
        quote: "John helped me to build and integrated an online B2B and B2C marketplace. I would like to thanks experienced ecommerce development team, who delivered effective solution and guidance.",
        company: "Captian America",
        name: "Steve Rogers",
        skill: "React"
    },
    {
        title: "React Tracks",
        subtitle: "React and Python",
        description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium dolore rerum laborum iure enim sint nemo omnis voluptate exercitationem eius?",
        image: "./project-2.gif",
        link: "https://reedbarger.com",
        quote: "John helped me to build and integrated an online B2B and B2C marketplace. I would like to thanks experienced ecommerce development team, who delivered effective solution and guidance.",
        company: "Hulk",
        name: "Dr Banner",
        skill: "Angular Js"
    },
    {
        title: "DevChat",
        subtitle: "React and Firebase",
        description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium dolore rerum laborum iure enim sint nemo omnis voluptate exercitationem eius?",
        image: "./project-3.gif",
        link: "https://jsbootcamp.com",
        quote: "John helped me to build and integrated an online B2B and B2C marketplace. I would like to thanks experienced ecommerce development team, who delivered effective solution and guidance.",
        company: "Ironman",
        name: "Tony Stark",
        skill: "PHP"
    },
    {
        title: "Epic Todo App",
        subtitle: "React Hooks",
        description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium dolore rerum laborum iure enim sint nemo omnis voluptate exercitationem eius?",
        image: "./project-4.gif",
        link: "https://pythonbootcamp.com",
        quote: "John helped me to build and integrated an online B2B and B2C marketplace. I would like to thanks experienced ecommerce development team, who delivered effective solution and guidance.",
        company: "Black Widow",
        name: "Natasha Romnoroff",
        skill: "Node Js"
    },
];