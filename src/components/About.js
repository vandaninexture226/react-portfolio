import React from "react";

export default function About() {
    return (
        <section id="about" className="text-gray-400 bg-blue-900 body-font">
            <div className="container mx-auto flex px-10 py-20 md:flex-row flex-col items-center">
                <div className="lg:flex-grow md:w-1/2 lg:pr-24 md:pr-16 flex flex-col md:items-start md:text-left mb-16 md:mb-0 items-center text-center">
                    <h1 className="title-font sm:text-4xl text-3xl mb-4 font-medium text-white">
                        Hii, I'm John Carter.
                        <br className="hidden lg:inline-block"/> I love to build amazing apps.
                    </h1>
                    <p className="mb-8 leading-relaxed">
                        Experienced FullStack Developer with a demonstrated history of working in the information technology and services industry. 
                        Strong engineering professional skilled in Angular 4/6/8,AngularJs,JavaScript,React,HTML,CSS,PHP,Laravel, Yii2, MySql and MongoDB.
                    </p>
                    <div className="flex justify-center">
                        <a href="#contact" className="text-white bg-indigo-500 border-0 py-2 px-6 focus:outline-none hover:bg-indigo-600 rounded text-lg">
                            Work with Me
                        </a>
                        <a href="#projects" className="ml-4 inline-flex text-gray-400 bg-gray-800 border-0 py-2 px-6 focus:outline-none hover:bg-gray-700 hover:text-white rounded text-lg">
                            See My Past Work
                        </a>                    
                    </div>
                </div>
                <div className="lg:max-w-lg lg:w-full md:w-1/2 w-5/6">
                    <img
                        className="object-cover object-center rounded"
                        alt="hero"
                        src="./Che_Guevara_vector_SVG_format.svg" />
                </div>
            </div>
        </section>
    );
}